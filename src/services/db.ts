import Dexie from 'dexie';

type noteDate = {
    name: string,
    text: string
}

export default class NotesDatabase extends Dexie {
  notes: Dexie.Table<INote, number>;

  constructor() {  
    super("NotesDatabase");

    // Определение таблицы
    this.version(1).stores({
      notes: '++id, name, text'
    });

    this.notes = this.table("notes");    
  }

  // Добавление заметки 
  addNote = async (noteDate: noteDate) => {
    await db.notes.add({
      name: noteDate.name, text: noteDate.text		
    }); 
  }

  // Обновление названия заметки 
  updateNoteName = async (id: number, name: string) => {
    await db.transaction('rw', db.notes, async () => {
      db.notes.update(id, {name: name}).then(function (updated) {
        if (updated)
          console.log ("Название заметки успешно обновлено");
        else
          console.log ("Ошибка обновления названия или просто оно не изменилась!");
      });
    });
  }

  // Обновление текста заметки 
  updateNoteText = async (id: number, text: string) => {
    await db.transaction('rw', db.notes, async () => {
      db.notes.update(id, {text: text}).then(function (updated) {
        if (updated)
          console.log ("Содержимое заметки успешно обновлено");
        else
          console.log ("Ошибка обновления содержимого заметки или просто она не изменилась!");
      });
    });
  }

  // Получение всех заметок 
  getAll = async () => {
    let notesDb: INote[] = [];
    await db.transaction('r', db.notes, async () => {
      notesDb = await db.notes.where('id').above(0).toArray();
    });

    return notesDb;
  }

  getNoteById = async (id: number) => {
    let noteDb: INote[] = [];
    await db.transaction('r', db.notes, async () => {
      noteDb = await db.notes.where('id').equals(id).toArray();
    });

    return noteDb;
  }

  deleteNoteById = async (id: number) => {
    await db.transaction('rw', db.notes, async () => {
      db.notes.where('id').equals(id).delete().then(function(count) {
        console.log("Запись упешно удалена: " + count);
      }).catch(function(error) {
        console.log("Ошибка удаления из базы данных: " + error);
      })
    });
  }
}
  
// Создаем интерфейс табличных записей, Вы получаете лучший тип безопасности и кода
export interface INote {
  id?: number; // Первичный ключ
  name: string; // Название заметки
  text: string; // Контент заметки
}

export var db = new NotesDatabase();
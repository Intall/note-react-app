import React, {Component, MouseEvent} from 'react'
import { db } from './services/db'
import { AppContextProvider } from './contexts/notes-context'
import filterAsync from 'node-filter-async'
import { Layout, Modal } from 'antd'
import SideBar from './components/SideBar'
import WorkSpace from './components/WorkSpace'

import 'antd/dist/antd.css'

const WAIT_INTERVAL = 1000
const AUTO_SAVE_ON = true

export default class App extends Component {
  public state = {
    statusWorkSpace: 'clean', // clean, edit, add, open
    saveStatus: 'ok', // ok, saving,
    searchText: '',
    newNote: {
      id: 0,
      name: '',
      text: '',
    },
    editNote: {
      id: 0,
      name: '',
      text: '',
    },
    notes: [],
    searchNotes: [],
  }

  public timer: any

  // Создание компонента и обновление списка заметок
  public componentDidMount() {
    this.updateNotesStatusFromDb()
  }

  // Меняем статус рабочей области при нажатии на кнопку Новая заметка
  public showWorkSpaceForEdit = () => {
    this.setState({
      newNote: {
        name: '',
        text: '',
      },
      statusWorkSpace: 'add',
    })
  }

  // Добавдяем в базу новую заметку и обновляем список заметок
  public addNewNoteAction = async () => {
    if (this.state.newNote.name && this.state.newNote.text) {
      await db.addNote(this.state.newNote)
      this.updateNotesStatusFromDb()
      this.setState({
        statusWorkSpace: 'clean',
      })
    } else {
      Modal.warning({
        title: 'Внимание',
        content: 'Заполните название и текст заметки',
      })
    }
  }

  // Сохранение принудительное (всех полей) так как есть автосохранение
  // при нажатии на кнопку сохранить
  // Проверить что работает можно увеличив время задержки автосохранения
  // например на WAIT_INTERVAL = 10000
  public saveNoteAction = () => {
    this.updateNameDb()
    this.updateTextDb()
  }

  // Получение всех заметок из БД и обновление стейта
  public updateNotesStatusFromDb = async () => {
    const notesDb = await db.getAll()
    this.setState({
      newNote: {
        ...this.state.newNote,
      },
      editNote: {
        ...this.state.editNote,
      },
      notes: notesDb,
    })
  }

  // Создаем пустой таймер для того чтобы на его основе сделать задержку автосохранения
  public componentWillMount() {
    this.timer = null
  }

  // Событие изменения текста в поле: Название
  public onChangeNameAction = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (this.state.statusWorkSpace === 'add') {
      this.setState({
        newNote : {
          ...this.state.newNote,
          name: event.target.value,
        },
      })
    } else if (this.state.statusWorkSpace === 'edit') {
      this.setState({
        editNote : {
          ...this.state.editNote,
          name: event.target.value,
        },
      })
    }

    // Автосохранение с задержкой
    if (AUTO_SAVE_ON) {
      this.setState({
        saveStatus: 'saving',
      })

      if (this.state.statusWorkSpace === 'edit') {
        clearTimeout(this.timer)
        this.timer = setTimeout(this.updateNameDb, WAIT_INTERVAL)
      }
    }
  }

  // Редактирование Названия заметки в БД
  public updateNameDb = async () => {
    await db.updateNoteName(this.state.editNote.id, this.state.editNote.name)
    await this.updateNotesStatusFromDb()
    this.setState({
      saveStatus: 'ok',
    })
  }

  // Событие изменения текста в поле: Текст
  // Первый вариант был для Text Area
  public onChangeTextAction = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    if (this.state.statusWorkSpace === 'add') {
      this.setState({
        newNote : {
          ...this.state.newNote,
          text: event.target.value,
        },
      })
    } else if (this.state.statusWorkSpace === 'edit') {
      this.setState({
        editNote : {
          ...this.state.editNote,
          text: event.target.value,
        },
      })
    }

    // Автосохранение с задержкой
    if (AUTO_SAVE_ON) {
      this.setState({
        saveStatus: 'saving',
      });
      if (this.state.statusWorkSpace === 'edit') {
        clearTimeout(this.timer)
        this.timer = setTimeout(this.updateTextDb, WAIT_INTERVAL)
      }
    }
  }

  // Событие изменения текста в поле: Текст
  public onChangeTextMdAction = (str: string) => {
    if (this.state.statusWorkSpace === 'add') {
      this.setState({
        newNote : {
          ...this.state.newNote,
          text: str,
        },
      })
    } else if (this.state.statusWorkSpace === 'edit') {
      this.setState({
        editNote : {
          ...this.state.editNote,
          text: str,
        },
      })
    }

    // Автосохранение с задержкой
    if (AUTO_SAVE_ON) {
      this.setState({
        saveStatus: 'saving',
      })
      if (this.state.statusWorkSpace === 'edit') {
        clearTimeout(this.timer)
        this.timer = setTimeout(this.updateTextDb, WAIT_INTERVAL)
      }
    }
  }

  // Редактирование Текста заметки в  БД
  public updateTextDb = async () => {
    await db.updateNoteText(this.state.editNote.id, this.state.editNote.text)
    await this.updateNotesStatusFromDb()
    this.setState({
      saveStatus: 'ok',
    })
  }

  // Загрузка заметки в рабочую область при нажатии на элемент в списке заметок
  public openNoteAction = async (event: MouseEvent) => {
    event.preventDefault()
    const noteId = parseInt(event.currentTarget.getAttribute('data-id') || '0', 10);

    // Загрузим из базы мало ли кто поменял state
    const selectNoteDb = await db.getNoteById(noteId)

    this.setState({
      editNote : {
        id: selectNoteDb[0].id,
        name: selectNoteDb[0].name,
        text: selectNoteDb[0].text,
      },
      statusWorkSpace: 'open',
    })
  }

  // Открываем доступ к редактированию в рабочей области
  public editOpenNoteAction = async (event: MouseEvent) => {
    event.preventDefault()

    this.setState({
      statusWorkSpace: 'edit',
    })
  }

  // Удаляем заметку с подтверждением
  public deleteOpenNoteAction = async () => {
    const deleteId =  this.state.editNote.id
    await db.deleteNoteById(deleteId)

    this.hideWorkSpace()
    this.updateNotesStatusFromDb()
  }

  // Скрываем рабочую область при удалении или др ...
  public hideWorkSpace = () => {
    this.setState({
      statusWorkSpace: 'clean',
    })
  }

  // Событие изменения текста в поле: Текст
  public onChangeTextSearchAction = async (event: React.ChangeEvent<HTMLInputElement>) => {
    let searchQuery = event.target.value

    this.setState({
      searchText: searchQuery,
    })

    searchQuery = searchQuery.toString().toLowerCase()

    const allNotes = this.state.notes

    if (!this.isEmpty(searchQuery)) {
      // Использовал асинхронный фильтр
      const displayedNotes = await filterAsync(allNotes, async (item: {name: string, text: string}, index) => {
        let searchValue = ''
        if (item.name && item.name.length !== 0) {
          searchValue += item.name.toLowerCase()
        }
        if (item.text && item.text.length !== 0) {
          searchValue += ' ' + item.text.toLowerCase()
        }

        return searchValue.indexOf(searchQuery) !== -1
      })

      this.setState({
        searchNotes: displayedNotes,
      })
    }
  }

  // Для проверки пустых строк
  public isEmpty = (str: string) =>  {
    return (!str || 0 === str.length)
  }

  public render() {
    const statusWorkSpace = this.state.statusWorkSpace;
    return (
      <AppContextProvider
        value={{
          showWorkSpace: this.showWorkSpaceForEdit,
          onChangeName: this.onChangeNameAction,
          onChangeTextMd: this.onChangeTextMdAction,
          onChangeTextSearch: this.onChangeTextSearchAction,
          addNewNote: this.addNewNoteAction,
          saveNote: this.saveNoteAction,
          openItem: this.openNoteAction,
          editOpenNote: this.editOpenNoteAction,
          deleteOpenNote: this.deleteOpenNoteAction,
          notes: (this.isEmpty(this.state.searchText)) ? this.state.notes : this.state.searchNotes,
          editNote: this.state.editNote,
          saveStatus: this.state.saveStatus,
          autoSaveOn: AUTO_SAVE_ON,
        }}
      >
        <Layout style={{ minHeight: '100vh'}}>
          <SideBar/>
          {statusWorkSpace !== 'clean' && <WorkSpace status={statusWorkSpace} />}
        </Layout>
      </AppContextProvider>
    )
  }
}
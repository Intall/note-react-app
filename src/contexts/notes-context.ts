import React, { ChangeEvent, MouseEvent } from 'react'

export interface MyContextProps {
  notes: Note[]
  editNote: {
    id: number
    name: string
    text: string
  }
  saveStatus: string
  autoSaveOn: boolean
  showWorkSpace(): any
  addNewNote(): any
  saveNote(): any
  openItem(event: MouseEvent): any
  editOpenNote(event: MouseEvent): any
  deleteOpenNote(): any
  onChangeName(event: ChangeEvent<HTMLInputElement>): any
  onChangeText(event: ChangeEvent<HTMLTextAreaElement>): any
  onChangeTextMd(str: string): any
  onChangeTextSearch(event: ChangeEvent<HTMLInputElement>): any
}

export interface Note {
  id: number
  name: string
  text: string
}

const appContext = React.createContext<Partial<MyContextProps>>({})

export const AppContextProvider = appContext.Provider
export const AppContextConsumer = appContext.Consumer
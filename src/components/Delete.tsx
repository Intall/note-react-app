import React, { Component } from 'react'
import { Button, Modal } from 'antd'

const { confirm } = Modal

interface Props {
  actionButton?(): any,
  showDeleteConfirm?(): any
}

export default class Delete extends Component<Props> {
  public render() {
    const {actionButton} = this.props

    const showDeleteConfirm = (func: any) => {
      confirm({
        title: 'Вы действительно хотите удалить заметку?',
        okText: 'Да',
        okType: 'danger',
        cancelText: 'Отмена',
        onOk() {
          func()
        },
        onCancel() {},
      })
    }

    return (
      <Button 
        type="danger"
        icon="delete"
        onClick={() => showDeleteConfirm(actionButton)}
      >
        Удалить
      </Button>
    )
  }
}
import React, { Component } from 'react'
import { Button } from 'antd'

interface Props {
  text: string,
  saveStatus?: string,
  actionButton?(): any
}

export default class SaveAddButton extends Component<Props> {
  public render() {
    const {text, actionButton, saveStatus} = this.props
    let statusButton = false
    let textButton = text
    if (saveStatus && saveStatus === 'ok') {
      statusButton = false
    } else if (saveStatus && saveStatus === 'saving') {
      statusButton = true
      textButton = 'Сохранение'
    }

    return (
      <Button
        type="primary"
        icon="save"
        loading={statusButton}
        onClick={actionButton}
      >
        {textButton}
      </Button>
    )
  }
}
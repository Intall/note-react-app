import React, { Component } from 'react'
import { Layout, Input, Alert, Typography } from 'antd'
import Edit from './Edit'
import Delete from './Delete'
import SaveAddButton from './SaveAddButton'
import { AppContextConsumer } from '../contexts/notes-context'
import SimpleMDE from 'react-simplemde-editor'
import ReactMarkdown from 'react-markdown'
import 'easymde/dist/easymde.min.css'

const { Title } = Typography
const { Content} = Layout

interface Props {
  status: string
}

export default class WorkSpace extends Component<Props> {
  public render() {
    const {status} = this.props
    let button
    let title
    let text

    if (status === 'add') {
      button =
        <AppContextConsumer>
          {
            ({addNewNote}) => (
              <SaveAddButton
                text="Добавить"
                actionButton={addNewNote}
              />
            )
          }
        </AppContextConsumer>

      title =
        <AppContextConsumer>
          {
            ({onChangeName}) => (
              <Input
                className="inputTitle"
                placeholder="Название заметки"
                style={{ margin: 16 }}
                onChange={onChangeName}
              />
            )
          }
        </AppContextConsumer>

      text =
        <AppContextConsumer>
          {
            ({onChangeTextMd}) => (
              <SimpleMDE
                onChange={onChangeTextMd}
                value=''
                options={{
                  spellChecker: false,
                }}
              />
            )
          }
        </AppContextConsumer>
    } else if (status === 'open') {
      button =
        <AppContextConsumer>
          {
            ({editOpenNote, deleteOpenNote}) => (
              <>
                <Edit actionButton={editOpenNote}/>
                <span className="mr10"/>
                <Delete actionButton={deleteOpenNote}/>
              </>
            )
          }
        </AppContextConsumer>

      title =
        <AppContextConsumer>
          {
            ({editNote}) => (
              <Title level={2} style={{ margin: 16 }}>
                {(editNote) ? editNote.name : ''}
              </Title>
            )
          }
        </AppContextConsumer>

      text =
        <AppContextConsumer>
          {
            ({editNote}) => (
              <ReactMarkdown
                source={(editNote) ? editNote.text : ''}
                escapeHtml={false}
              />
            )
          }
        </AppContextConsumer>

    } else if (status === 'edit') {
      button =
        <AppContextConsumer>
          {
            ({autoSaveOn, saveNote, deleteOpenNote, saveStatus}) => (
              <>
                <Alert
                  message={(autoSaveOn) ? "Автосохранение включено" : "Автосохранение выключенно"}
                  type={(autoSaveOn) ? "success" : "error"}
                  showIcon={true}
                  banner={true}
                  style={{width: 250, float: 'left', margin: '0 10px 0 0'}}
                />
                <SaveAddButton
                  text="Сохранить"
                  actionButton={saveNote}
                  saveStatus={saveStatus}
                />
                <span className="mr10"/>
                <Delete actionButton={deleteOpenNote}/>
              </>
            )
          }
        </AppContextConsumer>

      title =
        <AppContextConsumer>
          {
            ({onChangeName, editNote}) => (
              <Input
                className="inputTitle"
                placeholder="Название заметки"
                value={(editNote) ? editNote.name : ''}
                style={{ margin: 16 }}
                onChange={onChangeName}
              />
            )
          }
        </AppContextConsumer>

      text =
        <AppContextConsumer>
          {
            ({onChangeTextMd, editNote}) => (
              <SimpleMDE 
                onChange={onChangeTextMd}
                value={(editNote) ? editNote.text : ''}
                options={{
                  spellChecker: false,
                }}
              />
            )
          }
        </AppContextConsumer>
    }

    return (
      <Content style={{ margin: '0 16px' }}>
        <div style={{ display: 'flex', alignItems : 'center' }}>
          {title}
        </div>
        <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
          {text}
        </div>
        <div style={{ padding: 24, background: '#fff'}}>
          {button}
        </div>
      </Content>
    )
  }
}
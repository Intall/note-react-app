import React, { Component, MouseEvent } from 'react'
import { Button } from 'antd'

interface Props {
  actionButton?(event: MouseEvent): any
}

export default class Edit extends Component<Props> {
  public render() {
    const {actionButton} = this.props

    return (
      <Button
        type="primary"
        icon="edit"
        onClick={actionButton}
      >
        Редактировать
      </Button>
    )
  }
}
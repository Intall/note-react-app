import React, { Component } from 'react'
import { List, Menu, Icon } from 'antd'
import { AppContextConsumer } from '../contexts/notes-context'
import removeMd from 'remove-markdown'

export default class ListItems extends Component {
  public cleanText = (text: string): string => {
    return (text.length > 18) ? removeMd(text.substring(0, 18) + '...') : removeMd(text)
  }

  public render() {
    return (
      <AppContextConsumer>
        {
          ({showWorkSpace, notes, openItem}) => (
            <>
              <Menu className="new-note-button-wrap">
                {/* Menu.Item есть подержка onclick ts ругается напрасно
                // @ts-ignore */}
                <Menu.Item key="1" onClick={showWorkSpace} className="new-note-button">
                  <Icon type="plus-square" />
                    Новая заметка
                </Menu.Item>
              </Menu>
              <List
                itemLayout="horizontal"
                dataSource={notes}
                renderItem={
                  item => (
                    <div
                      data-id={item.id}
                      onClick={openItem}
                      className="item-list-note"
                    >
                        <List.Item>
                          <List.Item.Meta
                            style={{height : 50, overflow : 'hidden'}}
                            title={item.name}
                            description={this.cleanText(item.text)}
                          />
                        </List.Item>
                    </div>
                  )
                }
              />
            </>
          )
        }
      </AppContextConsumer>
    )
  }
}
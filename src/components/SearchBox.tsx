import React, { Component } from 'react'
import { Input } from 'antd'
import { AppContextConsumer } from '../contexts/notes-context'

const { Search } = Input

export default class SearchBox extends Component {
  public render() {
    return (
      <AppContextConsumer>
        {
          ({onChangeTextSearch}) => (
            <Search
              placeholder="Введите текст для поиска"
              onChange={onChangeTextSearch}
            />
          )
        }
      </AppContextConsumer>
    )
  }
}
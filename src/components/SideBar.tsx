import React, { Component } from 'react'
import SearchBox from './SearchBox'
import ListItem from './ListItem'
import { Layout} from 'antd'
import { AppContextConsumer } from '../contexts/notes-context'

const { Sider } = Layout

export default class SideBar extends Component {
  public render() {
    return (
      <AppContextConsumer>
        {
          () => (
            <Sider
              width={300}
              style={{background: '#fff', padding: 20, overflow : 'auto', height : 100 + 'vh'}}
            >
                <SearchBox />
                <ListItem/>
            </Sider>
          )
        }
      </AppContextConsumer>
    )
  }
}